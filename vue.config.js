const { WebpackManifestPlugin } = require('webpack-manifest-plugin')
const nodeExternals = require('webpack-node-externals')
const webpack = require('webpack')

exports.chainWebpack = webpackConfig => {
  if (!process.env.VUE_APP_SSR) {
    webpackConfig
      .entry('app')
      .clear()
      .add('./src/entry-client.ts')
    return
  }

  webpackConfig
    .entry('app')
    .clear()
    .add('./src/entry-server.ts')

  webpackConfig.target('node')
  webpackConfig.output.libraryTarget('commonjs2')

  webpackConfig.plugin('manifest').use(new WebpackManifestPlugin())
  webpackConfig.externals(nodeExternals({ allowlist: /\.(css|vue)$/ }))
  webpackConfig.optimization.splitChunks(false).minimize(false)
  webpackConfig.plugins.delete('hmr')
  webpackConfig.plugins.delete('preload')
  webpackConfig.plugins.delete('prefetch')
  webpackConfig.plugins.delete('progress')
  webpackConfig.plugins.delete('friendly-errors')
  webpackConfig.plugin('limit').use(
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    })
  )
}
