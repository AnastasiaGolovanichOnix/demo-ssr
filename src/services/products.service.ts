import { AxiosResponse } from 'axios'

import ApiService from '@/services/_helper/api.service'
import { ReviewApiInterface } from '@/types/api/review.api.interface'

export default class ProductsService {
  static getProduct (): Promise<AxiosResponse> {
    return ApiService.get('/shops?populate=*')
  }

  static changeIsLike (payload: {
    id: number;
    isLike: boolean
  }): Promise<AxiosResponse> {
    return ApiService.put('/shops/' + payload.id, {
      data: {
        isLike: payload.isLike
      }
    })
  }

  static changeIsDislike (payload: {
    id: number;
    isDislike: boolean
  }): Promise<AxiosResponse> {
    return ApiService.put('/shops/' + payload.id, {
      data: {
        isDislike: payload.isDislike
      }
    })
  }

  static changeIsFavorite (payload: {
    id: number;
    isFavorite: boolean
  }): Promise<AxiosResponse> {
    return ApiService.put('/shops/' + payload.id, {
      data: {
        isFavorite: payload.isFavorite
      }
    })
  }

  static setReview (payload: {
    id: number;
    review: ReviewApiInterface[],
  }): Promise<AxiosResponse> {
    return ApiService.put('/shops/' + payload.id, {
      data: {
        review: payload.review
      }
    })
  }
}
