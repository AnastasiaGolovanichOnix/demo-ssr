import { AxiosResponse } from 'axios'

import ApiService from '@/services/_helper/api.service'

export default class AuthService {
  static login (payload: {
    identifier: string;
    password: string;
  }): Promise<AxiosResponse> {
    return ApiService.post('/auth/local', payload)
  }

  static forgotPassword (payload: {
      email: string;
      url: string;
    }): Promise<AxiosResponse> {
    return ApiService.post('/auth/forgot-password', payload)
  }

  static resetPassword (payload: {
      code: string;
      password: string;
      passwordConfirmation: string;
    }): Promise<AxiosResponse> {
    return ApiService.post('/auth/reset-password', payload)
  }

  static register (payload: {
      username: string;
      email: string;
      password: string;
  }): Promise<AxiosResponse> {
    return ApiService.post('/auth/local/register', payload)
  }
}
