import { AxiosResponse } from 'axios'

import ApiService from '@/services/_helper/api.service'

export default class FeaturesService {
  static getFeatures (): Promise<AxiosResponse> {
    return ApiService.get('/features?populate=*')
  }
}
