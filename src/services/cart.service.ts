import { AxiosResponse } from 'axios'

import ApiService from '@/services/_helper/api.service'

export default class CartService {
  static getCart (): Promise<AxiosResponse> {
    return ApiService.get('/carts')
  }

  static deleteCartItem (payload: {
    id: number;
  }): Promise<AxiosResponse> {
    return ApiService.delete('/carts/' + payload.id)
  }

  static postCartItem (payload: {
    data: {
      name: string;
      imageUrl: string;
      price: number
    }
  }): Promise<AxiosResponse> {
    return ApiService.post('/carts', payload)
  }
}
