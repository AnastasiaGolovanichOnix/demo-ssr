import axios from 'axios'
import store from '@/store'

const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 60 * 1000
})
axiosInstance.interceptors.request.use((config) => {
  const token = store.getters['user/getAccessToken']
  if (token) {
    const configAuthorized = { ...config }
    if (configAuthorized && configAuthorized.headers) {
      configAuthorized.headers.Authorization = `Bearer ${token}`
    }
    return configAuthorized
  }
  return config
})

export default axiosInstance
