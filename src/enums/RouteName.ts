export enum RouteName {
  Home = 'Home',
  Login = 'Login',
  Register = 'Register',
  Product = 'Product',
  ForgotPassword = 'ForgotPassword',
  ResetPassword = 'ResetPassword',
  ShoppingCard = 'ShoppingCard',
  WishList = 'WishList'
}
