import { FeatureApiInterface } from '@/types/api/feature.api.interface'

export interface FeatureState {
  feature: FeatureApiInterface[];
}
