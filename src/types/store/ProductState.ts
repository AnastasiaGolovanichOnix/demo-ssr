import { ProductApiInterface } from '@/types/api/product.api.interface'

export interface ProductState {
  product: ProductApiInterface[];
}
