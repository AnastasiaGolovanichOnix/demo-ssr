import { CartApiInterface } from '@/types/api/cart.api.interface'

export interface CartState {
  cart: CartApiInterface[];
}
