import UserInterface from '@/types/store/UserInterface'

export interface UserState {
  user: UserInterface;
  accessToken: string;
}
