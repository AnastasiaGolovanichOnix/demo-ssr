import { UserState } from '@/types/store/UserState'
import { CartState } from '@/types/store/CartState'
import { FeatureState } from '@/types/store/FeatureState'

export interface RootState {
  user: UserState;
  cart: CartState;
  feature: FeatureState;
}
