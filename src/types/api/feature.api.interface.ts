import { ImageApiInterface } from '@/types/api/image.api.interface'

export interface FeatureApiInterface {
    id: number,
    attributes: {
        title: string,
        description: string,
        createdAt?: string,
        updatedAt?: string,
        publishedAt?: string,
        imageUrl: string
        image: { data: ImageApiInterface }
    }
}
