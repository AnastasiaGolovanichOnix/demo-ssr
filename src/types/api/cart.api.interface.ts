export interface CartApiInterface {
    id: number,
    attributes: {
        name: string,
        imageUrl: string
        createdAt?: string,
        updatedAt?: string,
        publishedAt?: string,
        price: number,
    }
}
