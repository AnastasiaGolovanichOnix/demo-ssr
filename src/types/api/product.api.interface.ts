import { ReviewApiInterface } from '@/types/api/review.api.interface'
import { ImageApiInterface } from '@/types/api/image.api.interface'

export interface ProductApiInterface {
    id: number,
    attributes: {
        name: string,
        description: string,
        longDescription?: string,
        createdAt?: string,
        updatedAt?: string,
        publishedAt?: string,
        price: number,
        mainImageUrl: string,
        imagesUrl?: string,
        isLike: boolean,
        isDislike: boolean,
        isFavorite: boolean,
        uri: string
        reviews?: ReviewApiInterface[]
        specification?: string
        image?: {
            data: ImageApiInterface
        }
        images?: {
            data: ImageApiInterface[]
        }
    }
}
