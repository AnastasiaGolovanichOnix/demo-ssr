export interface ImageApiInterface {
    id: number
    attributes: {
        alternativeText: string
        caption: string
        createdAt: string
        ext: string
        formats: {
            small: {
                ext: string
                hash: string
                height: number
                mime: string
                name: string
                path: null
                size: number
                url: string
                width: number
            }
            thumbnail: {
                ext: string
                hash: string
                height: number
                mime: string
                name: string
                path: null
                size: number
                url: string
                width: number
            }
        }
        hash: string
        height: number
        mime: string
        name: string
        previewUrl: null
        provider: string
        providerMetadata?: null
        size: number
        updatedAt: string
        url: string
        width: number
    }
}
