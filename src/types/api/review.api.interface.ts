export interface ReviewApiInterface {
    id?: number,
    name: string,
    review: string,
    rating: number
}
