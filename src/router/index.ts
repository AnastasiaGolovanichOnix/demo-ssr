import { createMemoryHistory, createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { isSSR } from '@/helpers'
import store from '@/store'
import { RouteName } from '@/enums/RouteName'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "home" */ '../layouts/TheMainLayout.vue'),
    meta: { public: true },
    children: [
      {
        path: '',
        name: RouteName.Home,
        component: () => import(/* webpackChunkName: "main" */ '../views/HomePage.vue')
      },
      {
        path: 'categories',
        component: () => import(/* webpackChunkName: "categories" */ '../views/NotFound.vue')
      },
      {
        path: 'about',
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
      },
      {
        path: 'shop',
        component: () => import(/* webpackChunkName: "shop" */ '../views/Shop.vue')
      },
      {
        path: 'product/:uri/:id',
        name: RouteName.Product,
        props: true,
        component: () => import(/* webpackChunkName: "shop" */ '../views/ProductInfo.vue')
      },
      {
        path: 'contact',
        component: () => import(/* webpackChunkName: "contact" */ '../views/NotFound.vue')
      },
      {
        path: 'shopping-cart',
        name: RouteName.ShoppingCard,
        component: () => import(/* webpackChunkName: "shopping-cart" */ '../views/Cart.vue'),
        meta: { public: false }
      },
      {
        path: 'wish-list',
        name: RouteName.WishList,
        component: () => import(/* webpackChunkName: "wish-list" */ '../views/WishList.vue'),
        meta: { public: false }
      }
    ]
  },
  {
    path: '/auth',
    component: () =>
      import(
        /* webpackChunkName: "unauthorized-layout" */ '@/layouts/TheAuthorisationLayout.vue'
      ),
    meta: { public: true },
    children: [
      {
        path: 'login',
        name: RouteName.Login,
        component: () =>
          import(/* webpackChunkName: "login" */ '@/views/TheLogin.vue'),
        meta: { public: true }
      },
      {
        path: 'register',
        name: RouteName.Register,
        component: () =>
          import(/* webpackChunkName: "register" */ '@/views/TheRegister.vue'),
        meta: { public: true }
      },
      {
        path: 'forgot-password',
        name: RouteName.ForgotPassword,
        component: () =>
          import(/* webpackChunkName: "forgot-password" */ '@/views/ForgotPassword.vue'),
        meta: { public: true }
      },
      {
        path: 'reset-password',
        name: RouteName.ResetPassword,
        component: () =>
          import(/* webpackChunkName: "reset-password" */ '@/views/ResetPassword.vue'),
        meta: { public: true }
      }
    ]
  },
  {
    path: '/:catchAll(.*)',
    component: () => import(/* webpackChunkName: "not-found" */ '../views/NotFound.vue')
  }
]

const router = createRouter({
  history: isSSR() ? createMemoryHistory() : createWebHistory(process.env.BASE_URL),
  routes
})

export default router
