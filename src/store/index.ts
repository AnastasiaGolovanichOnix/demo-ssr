import { createStore } from 'vuex'

import userStore from './modules/user'
import cartStore from './modules/cart'
import shopStore from './modules/shop'
import featureStore from './modules/feature'
import { RootState } from '@/types/store/RootState'

export default createStore({
  state: {} as RootState,
  mutations: {},
  actions: {},
  modules: {
    user: userStore,
    cart: cartStore,
    shop: shopStore,
    feature: featureStore
  }
})
