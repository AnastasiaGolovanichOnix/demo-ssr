import { Module } from 'vuex'

import UserInterface from '@/types/store/UserInterface'
import { RootState } from '@/types/store/RootState'
import { UserState } from '@/types/store/UserState'

const getDefaultState = () => {
  return {
    user: {} as UserInterface,
    accessToken: ''
  } as UserState
}

const store: Module<UserState, RootState> = {
  namespaced: true,
  state: getDefaultState(),
  getters: {
    getAccessToken (state: UserState) {
      return state.accessToken
    },
    getUser (state: UserState) {
      return state.user
    }
  },
  mutations: {
    setAccessToken (state: UserState, accessToken: string) {
      state.accessToken = accessToken
    },
    setCurrentUser (state: UserState, user: UserInterface) {
      state.user = user
    },
    resetState (state: UserState) {
      state = Object.assign(state, getDefaultState())
    }
  },
  actions: {
    setAccessToken ({ commit }, res) {
      commit('setAccessToken', res)
    },
    setCurrentUser ({ commit }, res) {
      commit('setCurrentUser', res)
    },
    logout ({ commit }) {
      commit('resetState')
    }
  }
}

export default store
