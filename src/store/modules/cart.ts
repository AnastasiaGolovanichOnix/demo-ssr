import { Module } from 'vuex'

import { RootState } from '@/types/store/RootState'
import { CartState } from '@/types/store/CartState'
import { CartApiInterface } from '@/types/api/cart.api.interface'

const getDefaultState = () => {
  return {
    cart: [] as CartApiInterface[]
  } as CartState
}

const store: Module<CartState, RootState> = {
  namespaced: true,
  state: getDefaultState(),
  getters: {
    getCart (state: CartState) {
      return state.cart
    },
    getCartSize (state: CartState) {
      return state.cart.length
    },
    getTotalCartPrice (state: CartState) {
      let price = 0
      state.cart.forEach((item) => {
        price += item.attributes.price
      })
      return price
    }
  },
  mutations: {
    setCart (state: CartState, item: CartApiInterface[]) {
      state.cart = item
    },
    removeCartItem (state: CartState, itemId: number) {
      state.cart = state.cart.filter((item) => item.id !== itemId)
    },
    resetState (state: CartState) {
      state = Object.assign(state, getDefaultState())
    }
  },
  actions: {
    setCart ({ commit }, res) {
      commit('setCart', res)
    },
    removeCartItem ({ commit }, res) {
      commit('removeCartItem', res)
    },
    clearCart ({ commit }) {
      commit('resetState')
    }
  }
}

export default store
