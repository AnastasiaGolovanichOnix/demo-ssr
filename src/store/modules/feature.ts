import { Module } from 'vuex'

import { RootState } from '@/types/store/RootState'
import { FeatureApiInterface } from '@/types/api/feature.api.interface'
import { FeatureState } from '@/types/store/FeatureState'

const getDefaultState = () => {
  return {
    feature: [] as FeatureApiInterface[]
  } as FeatureState
}

const store: Module<FeatureState, RootState> = {
  namespaced: true,
  state: getDefaultState(),
  getters: {
    getFeature (state: FeatureState) {
      return state.feature
    }
  },
  mutations: {
    setFeature (state: FeatureState, item: FeatureApiInterface[]) {
      state.feature = item
    }
  },
  actions: {
    setFeature ({ commit }, res) {
      commit('setFeature', res)
    }
  }
}

export default store
