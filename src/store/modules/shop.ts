import { Module } from 'vuex'

import { RootState } from '@/types/store/RootState'
import { ProductApiInterface } from '@/types/api/product.api.interface'
import { ProductState } from '@/types/store/ProductState'

const getDefaultState = () => {
  return {
    product: [] as ProductApiInterface[]
  } as ProductState
}

const store: Module<ProductState, RootState> = {
  namespaced: true,
  state: getDefaultState(),
  getters: {
    getProduct (state: ProductState) {
      return state.product
    },
    getShopSize (state: ProductState) {
      return state.product.length
    },
    getProductById: (state: ProductState) => (id: number) => {
      return state.product.filter(item => item.id === Number(id))[0]
    },
    getWishList (state: ProductState) {
      return state.product.filter((item) => item.attributes.isFavorite)
    },
    getWishListCount (state: ProductState) {
      return state.product.filter((item) => item.attributes.isFavorite).length
    }
  },
  mutations: {
    setProduct (state: ProductState, item: ProductApiInterface[]) {
      state.product = item
    },
    changeIsLike (state: ProductState, itemId: number) {
      state.product.filter((item) => item.id === itemId)[0].attributes.isLike = !state.product.filter((item) => item.id === itemId)[0].attributes.isLike
    },
    changeIsDislike (state: ProductState, itemId: number) {
      state.product.filter((item) => item.id === itemId)[0].attributes.isDislike = !state.product.filter((item) => item.id === itemId)[0].attributes.isDislike
    },
    changeIsFavorite (state: ProductState, itemId: number) {
      state.product.filter((item) => item.id === itemId)[0].attributes.isFavorite = !state.product.filter((item) => item.id === itemId)[0].attributes.isFavorite
    },
    resetState (state: ProductState) {
      state = Object.assign(state, getDefaultState())
    }
  },
  actions: {
    setProduct ({ commit }, res) {
      commit('setProduct', res)
    },
    changeIsLike ({ commit }, id) {
      commit('changeIsLike', id)
    },
    changeIsDislike ({ commit }, id) {
      commit('changeIsDislike', id)
    },
    changeIsFavorite ({ commit }, id) {
      commit('changeIsFavorite', id)
    },
    clearCart ({ commit }) {
      commit('resetState')
    }
  }
}

export default store
