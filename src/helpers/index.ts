import store from '@/store'

export function isSSR (): boolean {
  return typeof window === 'undefined'
}
export function isAuth (): boolean {
  return store?.getters['user/getAccessToken']
}
