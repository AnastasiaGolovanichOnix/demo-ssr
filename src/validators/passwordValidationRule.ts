const passwordValidationRule = (value: string) => {
  return value.length >= 6
}
export default passwordValidationRule
