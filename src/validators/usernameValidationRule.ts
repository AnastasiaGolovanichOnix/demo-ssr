const usernameValidationRule = (value: string) => {
  return value.length >= 3
}
export default usernameValidationRule
