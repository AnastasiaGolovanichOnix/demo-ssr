import { ReviewApiInterface } from '@/types/api/review.api.interface'

const reviewValidationRule = (review: ReviewApiInterface) => {
  return review.name && review.review && review.rating
}
export default reviewValidationRule
