const emailValidationRule = (value: string) => {
  return /^[A-Z\d.!#$%&'*+=?^_`{|}~-]+@[A-Z\d.-]+\.[A-Z]{2,}$/i.test(value)
}
export default emailValidationRule
