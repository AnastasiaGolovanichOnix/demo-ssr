const confirmPasswordValidationRule = (password: string, confirmPassword: string) => {
  return password === confirmPassword
}
export default confirmPasswordValidationRule
