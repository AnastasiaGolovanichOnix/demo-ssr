# demo-ssr

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### serverside Builds
```
yarn build:ssr
```

### Lints and fixes files
```
yarn lint
```

### Use Strapi for API
```
email - anastasiia.holovanych@onix-systems.com
password - Qwerty12345

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
